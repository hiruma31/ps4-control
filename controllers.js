const {Device} = require('ps4-waker');
var ps4 = new Device();
ps4.openSocket();

const fs = require('fs');
try {
    var titles = require('./titles.json')
} catch(err) {
    console.log("Couldn't read titles.json")
    fs.writeFileSync('./titles.json', '{}')
    var titles = {}
}

// Not the fanciest but too lazy for now
var cron = require('node-cron');
cron.schedule('*/1 * * * *', () => {
    console.log("Cron triggered")
    if (ps4.isConnected) {
        ps4.getDeviceStatus().then(device => extractTitleFromStatus(device)).catch();
    }
});

async function extractTitleFromStatus(device) {
    console.log("Running " + device['running-app-name'] + " ("+device['running-app-titleid']+")");
    titles[device['running-app-name']] = device['running-app-titleid'];
    // fs.writeFileSync('./titles.json', titles);

    let data = JSON.stringify(titles, null, 2);
    fs.writeFile('./titles.json', data, (err) => {
        if (err) throw err;
        console.log('Data written to file');
    });
}

exports.getStatus = (req, res) => {
    if (ps4.isConnected) {
        ps4.getDeviceStatus().then(res.json.bind(res)).catch();
    } else {
        res.json({"error": "PS4 not connected. Please try again later"});
    }
};

// To catch and timeout
exports.wakeUp = (req, res) => {
    ps4.turnOn().then(res.end.bind(res)).catch();
};

// To catch and timeout
exports.standBy = (req, res) => {
    ps4.turnOff()
        .then(res.json.bind(res))
        .catch();
};


exports.startTitle = (req, res) => {
    if (req.query.id) {
        ps4.startTitle(req.query.id)
            .then(res.json.bind(res))
            .catch(err => {
                if(err.kind === 'ObjectId') {
                    return res.status(404).send({
                        message: "Note not found with id " + req.query.id
                    });                
                }
            });
    } else if (req.params.name) {
        console.log("Starting " + req.params.name);
        if (req.params.name in titles) {
            ps4.startTitle(titles[req.params.name])
            .then(res.json.bind(res))
            .catch(err => {
                if(err.kind === 'ObjectId') {
                    return res.status(404).send({
                        message: "Failed to start Title by name"
                    });                
                }
            });
        } else {
            res.json({"Not found": req.params.name})
        }
    } else {
        res.status(401).json({"error": "Title ID not provided "});
    }
};

// Retrieve and return all notes from the database.
exports.addTitle = (req, res) => {

};

// Find a single note with a noteId
exports.removeTitle = (req, res) => {

};

// // Update a note identified by the noteId in the request
// exports.update = (req, res) => {

// };

// // Delete a note with the specified noteId in the request
// exports.delete = (req, res) => {

// };
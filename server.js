

// ps4.turnOn().then(() => ps4.startTitle("CUSA01850"));

const express = require('express');
const bodyParser = require('body-parser');

// create express webserver
const webserver = express(),
    port = process.env.PORT || 3000;
// parse requests of content-type - application/x-www-form-urlencoded
// webserver.use(bodyParser.urlencoded({ extended: true }))

// parse requests of content-type - application/json
webserver.use(bodyParser.json())
require('./routes.js')(webserver);
webserver.listen(port);


console.log('todo list RESTful API server started on: ' + port);
module.exports = (webserver) => {
    const ctrl = require('./controllers.js');
    // List all registered endpoints
    webserver.get('/',  function (req, res) {
        var array = [];
        webserver._router.stack.forEach(function(r){
            if (r.route && r.route.path){
              array.push(r.route.path)
            }
          });
        res.json(array)
    });
    
    // Get PS4 status
    webserver.get('/status', ctrl.getStatus);
    // Power ON
    webserver.get('/wake', ctrl.wakeUp);
    // Power ON
    webserver.get('/sleep', ctrl.standBy);
    // Start Title
    webserver.get('/title/:name?', ctrl.startTitle);
    // TODO Add Title to config
    webserver.put('/title/:name?', ctrl.addTitle);
    // TODO Remove Title from config
    webserver.delete('/title/:name?', ctrl.removeTitle);

    // Retrieve all Notes
    // webserver.get('/notes', notes.findAll);

    // // Retrieve a single Note with noteId
    // webserver.get('/title/:titleName?&:titleId?', ctrl.startTitle);

    // // Update a Note with noteId
    // webserver.put('/notes/:noteId', notes.update);

    // // Delete a Note with noteId
    // webserver.delete('/notes/:noteId', notes.delete);
}